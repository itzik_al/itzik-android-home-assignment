package homework.chegg.com.chegghomework.extantions

import org.junit.Assert.*
import org.junit.Test
import java.util.*

class DateHelperTest {

    class DateCreatorMock(private val date : Date) : DateCreator{
        override fun getDate(): Date  = date
    }

    @Test
    fun isPassPeriod_NotTimeSaved() {
        val nowDate = Date(10)
        val helper = DateHelper(DateCreatorMock(nowDate))

        val isPass = helper.isPassPeriod(3, null)

        assertTrue(isPass)
    }


    @Test
    fun isPassPeriod_NotPass() {
        val lastKnowdate = Date(0)
        val nowDate = Date(10)
        val helper = DateHelper(DateCreatorMock(nowDate))

        val isPass = helper.isPassPeriod(15, lastKnowdate)

        assertFalse(isPass)
    }

    @Test
    fun isPassPeriod_OnTheEage() {
        val lastKnowdate = Date(0)
        val nowDate = Date(10)
        val helper = DateHelper(DateCreatorMock(nowDate))

        val isPass = helper.isPassPeriod(10, lastKnowdate)

        assertFalse(isPass)
    }

    @Test
    fun isPassPeriod_Passed() {
        val lastKnowdate = Date(0)
        val nowDate = Date(10)
        val helper = DateHelper(DateCreatorMock(nowDate))

        val isPass = helper.isPassPeriod(5, lastKnowdate)

        assertTrue(isPass)
    }
}