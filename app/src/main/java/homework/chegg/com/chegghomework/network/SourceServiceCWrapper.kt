package homework.chegg.com.chegghomework.network

import homework.chegg.com.chegghomework.model.NewsItem
import homework.chegg.com.chegghomework.network.responses.mapToNewsItem

class SourceServiceCWrapper(private val serviceC: SourceCService) : SourceServiceWrapper {
    override suspend fun getSourceData(): List<NewsItem> {
        return  serviceC.getData().mapIndexed { index, it -> it.mapToNewsItem(index) }
    }
}