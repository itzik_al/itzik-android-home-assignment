package homework.chegg.com.chegghomework

import android.util.Log
import androidx.lifecycle.*
import homework.chegg.com.chegghomework.model.NewsItem
import homework.chegg.com.chegghomework.repositories.NewsFeedRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

private const val TAG = "MainActivityViewModel"
class MainActivityViewModel(private val newsFeedRepository: NewsFeedRepository) : ViewModel() {

    val newsFeed = newsFeedRepository.newsFeedList.map {
        it.sortedWith(compareBy(NewsItem::key, NewsItem::index))
    }
    val loading = MutableLiveData(false)
    fun fatchData() {
        viewModelScope.launch(Dispatchers.Main){
            loading.value = true
            Log.d(TAG, "fatchData(), started")
            newsFeedRepository.refreshAllNewsFeed()
            Log.d(TAG, "fatchData(), ended")
            loading.value = false
        }
    }
}