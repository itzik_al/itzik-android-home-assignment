package homework.chegg.com.chegghomework.extantions

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.constraintlayout.widget.Placeholder
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

@BindingAdapter("fromUrl", "placeHolder")
fun setImageViewSrcFromUrl(view: ImageView, fromUrl: String?, placeholder: Drawable) {
    Glide.with(view).load(fromUrl)
        .placeholder(placeholder)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(view)
}