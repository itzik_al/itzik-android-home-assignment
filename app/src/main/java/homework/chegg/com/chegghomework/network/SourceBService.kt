package homework.chegg.com.chegghomework.network

import homework.chegg.com.chegghomework.network.responses.SourceBResponse
import retrofit2.http.GET

interface SourceBService {
    @GET("android_homework_datasourceB.json")
    suspend fun getData() : SourceBResponse
}