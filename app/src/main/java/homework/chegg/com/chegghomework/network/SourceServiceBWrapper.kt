package homework.chegg.com.chegghomework.network

import homework.chegg.com.chegghomework.model.NewsItem
import homework.chegg.com.chegghomework.network.responses.mapToNewsItem

class SourceServiceBWrapper(private val serviceB: SourceBService) : SourceServiceWrapper {
    override suspend fun getSourceData(): List<NewsItem> {
        return  serviceB.getData().metadata.innerdata.mapIndexed { index, it -> it.mapToNewsItem(index) }
    }
}