package homework.chegg.com.chegghomework.network.responses

import homework.chegg.com.chegghomework.model.NewsItem

class SourceCResponse : ArrayList<SourceCResponseItem>()

data class SourceCResponseItem(
    val image: String?,
    val subLine1: String?,
    val subline2: String?,
    val topLine: String?
)

const val SOURCE_C_KEY = "C"
fun SourceCResponseItem.mapToNewsItem(index: Int) : NewsItem = NewsItem(SOURCE_C_KEY, index, image, topLine, "${subLine1?: ""}  ${subline2?:""}")