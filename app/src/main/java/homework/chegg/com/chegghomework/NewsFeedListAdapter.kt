package homework.chegg.com.chegghomework

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import homework.chegg.com.chegghomework.databinding.CardItemBinding
import homework.chegg.com.chegghomework.model.NewsItem

open class NewsFeedDiffUtill : DiffUtil.ItemCallback<NewsItem>(){
    override fun areItemsTheSame(
        oldItem: NewsItem,
        newItem: NewsItem
    ): Boolean {
        return oldItem.title == newItem.title
                && oldItem.subTitle == newItem.subTitle
                        && oldItem.image == newItem.image
    }

    override fun areContentsTheSame(
        oldItem: NewsItem,
        newItem: NewsItem
    ): Boolean {
        return oldItem == newItem
    }
}


class NewsFeedListAdapter : ListAdapter<NewsItem, NewsFeedViewHolder>(NewsFeedDiffUtill()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsFeedViewHolder {
        return NewsFeedViewHolder(
            CardItemBinding.inflate(LayoutInflater.from(parent.context),parent,
                                    false))
    }

    override fun onBindViewHolder(holder: NewsFeedViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

class NewsFeedViewHolder(private val binder : CardItemBinding) : RecyclerView.ViewHolder(binder.root) {
    fun bind(item: NewsItem) {
        binder.newsItem = item
    }

}