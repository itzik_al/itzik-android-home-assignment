package homework.chegg.com.chegghomework.repositories

import android.util.Log
import homework.chegg.com.chegghomework.database.SharedPref
import homework.chegg.com.chegghomework.extantions.DateHelper
import homework.chegg.com.chegghomework.model.NewsItem
import homework.chegg.com.chegghomework.network.SourceServiceWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import java.lang.Exception
import java.lang.RuntimeException
import java.util.*

private const val TAG = "SourceUpdateHelper"

class SourceUpdateHelper(private val sharedPref: SharedPref, private val dateHelper: DateHelper) {

    suspend fun createUpdateTask(sourceKey: String, staleTime: Long, service: SourceServiceWrapper) =
        withContext(Dispatchers.IO) {
            async<List<NewsItem>?> {
                try {
                    val lastUpdate = sharedPref.getSourceLastUpdate(sourceKey)
                    if (dateHelper.isPassPeriod(staleTime, lastUpdate)) {
                        Log.d(TAG, "refreshAllNewsFeed(), get $sourceKey started")
                        val data: List<NewsItem> = service.getSourceData()
                        sharedPref.saveSourceLastUpdate(sourceKey, Date())
                        return@async data
                    }
                    Log.d(TAG, "refreshAllNewsFeed(), get $sourceKey ended")

                } catch (e: Exception) {
                    Log.e(TAG, "refreshAllNewsFeed(), Failed for $sourceKey", e)
                }
                return@async null
            }
        }
}