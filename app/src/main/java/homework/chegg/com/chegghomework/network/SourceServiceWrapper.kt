package homework.chegg.com.chegghomework.network

import homework.chegg.com.chegghomework.model.NewsItem

interface SourceServiceWrapper{
    suspend fun getSourceData() : List<NewsItem>
}