package homework.chegg.com.chegghomework.database

import android.content.Context
import android.content.SharedPreferences
import java.util.*

const val SHARE_PREF_FILE_NAME = "shared_pref"
const val SOURCE_UPDATE_TAG = "source_"
class SharedPref(val context: Context) {


    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(SHARE_PREF_FILE_NAME, Context.MODE_PRIVATE)

    fun saveSourceLastUpdate(sourceTag :String, date : Date){
        sharedPreferences.edit().putLong("$SOURCE_UPDATE_TAG$sourceTag", date.time).apply()
    }

    fun getSourceLastUpdate(sourceTag :String) : Date?{
        val time = sharedPreferences.getLong("$SOURCE_UPDATE_TAG$sourceTag", 0)
        return if(time == 0L){
            null
        }else{
            Date(time)
        }
    }
}
