package homework.chegg.com.chegghomework.di

import homework.chegg.com.chegghomework.network.SourceAService
import homework.chegg.com.chegghomework.network.SourceBService
import homework.chegg.com.chegghomework.network.SourceCService
import org.koin.dsl.module
import retrofit2.Retrofit

val networkModule = module {

    single { RetrofitProvider.provideDefaultOkhttpClient(get()) }
    single { RetrofitProvider.provideRetrofit(get()) }
    single { provideApiSourceAService(RetrofitProvider.provideRetrofit(get())) }
    single { provideApiSourceBService(RetrofitProvider.provideRetrofit(get())) }
    single { provideApiSourceCService(RetrofitProvider.provideRetrofit(get())) }
}


fun provideApiSourceAService(retrofit: Retrofit): SourceAService = retrofit.create(
    SourceAService::class.java)

fun provideApiSourceBService(retrofit: Retrofit): SourceBService = retrofit.create(
    SourceBService::class.java)

fun provideApiSourceCService(retrofit: Retrofit): SourceCService = retrofit.create(
    SourceCService::class.java)
