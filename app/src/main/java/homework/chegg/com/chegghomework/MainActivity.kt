package homework.chegg.com.chegghomework

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel


private const val TAG = "MainActivity"
class MainActivity : AppCompatActivity() {
    private val viewModel by viewModel<MainActivityViewModel>()
    private val newsFeedListAdapter = NewsFeedListAdapter()
    private var toolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        buildUI()
        initObservers()
    }

    private fun initObservers() = with(viewModel) {
        newsFeed.observe(this@MainActivity) {
            newsFeedListAdapter.submitList(it)
            empty_state.visibility = if(it.isEmpty()) View.VISIBLE else View.GONE
        }
        loading.observe(this@MainActivity) {
            spinner.visibility = if(it) View.VISIBLE else View.GONE
            empty_state.visibility = if(it || !newsFeed.value.isNullOrEmpty()) View.GONE else View.VISIBLE
        }
    }

    private fun setupToolbar() {
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
    }

    private fun buildUI() {
        setContentView(R.layout.activity_main)
        setupToolbar()
        scaleView(empty_state_arrow, 0f,1f)
        my_recycler_view.adapter = newsFeedListAdapter
        my_recycler_view.layoutManager = LinearLayoutManager(this)
    }

    fun scaleView(v: View, startScale: Float, endScale: Float) {
        val anim: Animation = ScaleAnimation(
            startScale, endScale,  // Start and end values for the X axis scaling
            startScale, endScale,  // Start and end values for the Y axis scaling
            Animation.RELATIVE_TO_SELF, 0.5f,  // Pivot point of X scaling
            Animation.RELATIVE_TO_SELF, 0.5f
        ) // Pivot point of Y scaling
        anim.fillAfter = true // Needed to keep the result of the animation
        anim.duration = 1000

        anim.repeatMode =  Animation.RESTART
        anim.repeatCount = 100
        v.startAnimation(anim)
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_refresh -> {
                onRefreshData()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    // TODO fetch data from all data sources, aggregate data and display in RecyclerView
    private fun onRefreshData() {
        Toast.makeText(this, "Fetch data aggregate and show on RecyclerView", Toast.LENGTH_SHORT).show()
        viewModel.fatchData()
    }
}