package homework.chegg.com.chegghomework.database

import androidx.room.*
import homework.chegg.com.chegghomework.model.NewsItem

@Dao
abstract class NewsFeedDao : BaseDao<NewsItem>() {
    @Query("SELECT * FROM News")
    abstract override fun getAllData(): List<NewsItem>

    @Query("DELETE FROM news WHERE `key` =  :key")
    abstract fun deleteAllForKey(key: String)

}

@Database(entities = [NewsItem::class], version = 1, exportSchema = false)
abstract class NewsFeedDatabase : RoomDatabase() {
    abstract fun newsFeedDao(): NewsFeedDao
}
