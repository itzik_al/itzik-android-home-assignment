package homework.chegg.com.chegghomework

import java.util.concurrent.TimeUnit

object Consts {
    const val BASE_URL :String = "https://chegg-mobile-promotions.cheggcdn.com/android/homework/"
    const val DATA_SOURCE_A_URL = BASE_URL + "android_homework_datasourceA.json"
    const val DATA_SOURCE_B_URL = BASE_URL + "android_homework_datasourceB.json"
    const val DATA_SOURCE_C_URL = BASE_URL + "android_homework_datasourceC.json"

    val SOURCE_A_STALE_TIME: Long = TimeUnit.MINUTES.toMillis(5)
    val SOURCE_B_STALE_TIME: Long = TimeUnit.MINUTES.toMillis(30)
    val SOURCE_C_STALE_TIME: Long = TimeUnit.MINUTES.toMillis(60)

}