package homework.chegg.com.chegghomework.network

import homework.chegg.com.chegghomework.model.NewsItem
import homework.chegg.com.chegghomework.network.responses.mapToNewsItem

class SourceServiceAWrapper(private val serviceA: SourceAService) : SourceServiceWrapper {
    override suspend fun getSourceData(): List<NewsItem> {
        return serviceA.getData().stories.mapIndexed { index, it -> it.mapToNewsItem(index) }
    }
}