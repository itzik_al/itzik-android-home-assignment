package homework.chegg.com.chegghomework.extantions

import java.util.*

interface DateCreator{
    fun getDate() : Date
}

class DateCreatorImpl : DateCreator{
    override fun getDate() : Date = Date()
}

class DateHelper(private val dateCreator: DateCreator) {

    fun isPassPeriod(coolingPeriod : Long, lastTimeActivate: Date?) : Boolean{
        if (lastTimeActivate == null) return true
        val coolingEndAt = lastTimeActivate.time + coolingPeriod
        return coolingEndAt < dateCreator.getDate().time

    }
}