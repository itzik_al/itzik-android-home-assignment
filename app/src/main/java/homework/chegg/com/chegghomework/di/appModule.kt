package homework.chegg.com.chegghomework.di

import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import homework.chegg.com.chegghomework.MainActivityViewModel
import homework.chegg.com.chegghomework.database.NewsFeedDatabase
import homework.chegg.com.chegghomework.database.SharedPref
import homework.chegg.com.chegghomework.extantions.DateCreatorImpl
import homework.chegg.com.chegghomework.extantions.DateHelper
import homework.chegg.com.chegghomework.repositories.NewsFeedRepository
import homework.chegg.com.chegghomework.repositories.SourceUpdateHelper
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.bind
import org.koin.dsl.module

val appModule = module {

    factory { SharedPref(get()) }
    factory { SourceUpdateHelper(get(), get()) }
    factory { DateHelper(DateCreatorImpl()) }
    viewModel { MainActivityViewModel(get()) }

    single { get<NewsFeedDatabase>().newsFeedDao() }
    single { NewsFeedRepository(get(), get(), get(), get(),get())}
    single {
        Room.databaseBuilder(
            get(),
            NewsFeedDatabase::class.java, "LeaguesDatabase"
        )
            .addCallback(object : RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                }

                override fun onDestructiveMigration(db: SupportSQLiteDatabase) {
                    super.onDestructiveMigration(db)
                }

                override fun onOpen(db: SupportSQLiteDatabase) {
                    super.onOpen(db)
                }
            }).fallbackToDestructiveMigration().build()
    } bind  RoomDatabase::class
}