package homework.chegg.com.chegghomework.network.responses

import homework.chegg.com.chegghomework.model.NewsItem

data class SourceAResponse(
    val stories: List<Story>
)

data class Story(
    val imageUrl: String?,
    val subtitle: String?,
    val title: String?
)

const val SOURCE_A_KEY = "A"

fun Story.mapToNewsItem(index :Int) : NewsItem = NewsItem(SOURCE_A_KEY, index, imageUrl, title, subtitle)
