package homework.chegg.com.chegghomework.network.responses

import com.google.gson.annotations.SerializedName
import homework.chegg.com.chegghomework.model.NewsItem

data class SourceBResponse(
    val metadata: Metadata
)

data class Metadata(
    val innerdata: List<Innerdata>,
    @SerializedName("this")
    val _this: String
)

data class Innerdata(
    val articlewrapper: Articlewrapper,
    val aticleId: Int,
    val picture: String
)

data class Articlewrapper(
    val description: String,
    val header: String
)
const val SOURCE_B_KEY = "B"
fun Innerdata.mapToNewsItem(index : Int) : NewsItem = NewsItem(SOURCE_B_KEY, index, picture, articlewrapper.header, articlewrapper.description)