package homework.chegg.com.chegghomework.repositories

import androidx.lifecycle.MutableLiveData
import homework.chegg.com.chegghomework.Consts
import homework.chegg.com.chegghomework.database.NewsFeedDao
import homework.chegg.com.chegghomework.model.NewsItem
import homework.chegg.com.chegghomework.network.*
import homework.chegg.com.chegghomework.network.responses.SOURCE_A_KEY
import homework.chegg.com.chegghomework.network.responses.SOURCE_B_KEY
import homework.chegg.com.chegghomework.network.responses.SOURCE_C_KEY
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.withContext

private const val TAG = "NewsFeedRepository"

class NewsFeedRepository(
    private val serviceA: SourceAService,
    private val serviceB: SourceBService,
    private val serviceC: SourceCService,
    private val newsFeedDao: NewsFeedDao,
    private val sourceUpadate: SourceUpdateHelper
) {
    val newsFeedList = MutableLiveData<List<NewsItem>>()

    suspend fun refreshAllNewsFeed() = withContext(Dispatchers.IO) {

        val sourceA =
            sourceUpadate.createUpdateTask(SOURCE_A_KEY, Consts.SOURCE_A_STALE_TIME, SourceServiceAWrapper(serviceA))
        val sourceB =
            sourceUpadate.createUpdateTask(SOURCE_B_KEY, Consts.SOURCE_B_STALE_TIME, SourceServiceBWrapper(serviceB))
        val sourceC =
            sourceUpadate.createUpdateTask(SOURCE_C_KEY, Consts.SOURCE_C_STALE_TIME, SourceServiceCWrapper(serviceC))


        val updatelist = HashMap<String, Deferred<List<NewsItem>?>>()
        updatelist[SOURCE_A_KEY] = sourceA
        updatelist[SOURCE_B_KEY] = sourceB
        updatelist[SOURCE_C_KEY] = sourceC

        updatelist.values.awaitAll()

        updatelist.forEach {
            if (it.value.getCompleted()?.isNullOrEmpty() == false) {
                newsFeedDao.deleteAllForKey(it.key)
                newsFeedDao.insert(it.value.getCompleted())
            }
        }

        newsFeedList.postValue(newsFeedDao.getAllData())
    }
}

