package homework.chegg.com.chegghomework.network

import homework.chegg.com.chegghomework.network.responses.SourceAResponse
import retrofit2.http.GET

interface SourceAService {
    @GET("android_homework_datasourceA.json")
    suspend fun getData() : SourceAResponse
}