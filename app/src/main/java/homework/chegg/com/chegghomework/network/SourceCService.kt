package homework.chegg.com.chegghomework.network

import homework.chegg.com.chegghomework.network.responses.SourceCResponse
import retrofit2.http.GET

interface SourceCService {
    @GET("android_homework_datasourceC.json")
    suspend fun getData() : SourceCResponse
}