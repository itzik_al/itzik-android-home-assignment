package homework.chegg.com.chegghomework

import android.app.Application
import com.facebook.stetho.Stetho
import homework.chegg.com.chegghomework.di.appModule
import homework.chegg.com.chegghomework.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class TestApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this@TestApplication)
        initKoin()
    }
    private fun initKoin(){
        // start Koin (di) context
        startKoin {
            androidContext(this@TestApplication)
            androidLogger(Level.ERROR)

            modules(
                appModule,
                networkModule
            )
        }
    }

}