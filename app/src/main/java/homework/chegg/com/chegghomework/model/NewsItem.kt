package homework.chegg.com.chegghomework.model

import androidx.annotation.Keep
import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Keep
@Entity(tableName = "News")
data class NewsItem(
    val key: String,
    val index : Int,
    val image: String?,
    val title: String?,
    val subTitle: String?,

){
    @NonNull
    @PrimaryKey(autoGenerate = false)
    var id : String = "$key$index"
}